import './App.css';
import axios from 'axios';
import { useState } from 'react';
function App() {
  const [message, setMessage] = useState('')

  const handleCallApi = () => {
    axios(`/api`).then((res) => {
      setMessage(res.data.msg)
    })
      .catch((error) => {
        console.log('error:', error)
      })
  }
  return (
    <div className="App">
      <h3>Hello World...!</h3>
      <h4 className="blink_me">{message ? message : ''}</h4>
      <img className="lg" src="https://i.pinimg.com/originals/2a/ea/f3/2aeaf3a06ada257e9e3e387399abc60d.png" alt="" />
      <button className="btn btn-submit" onClick={handleCallApi}>Click me</button>
    </div>
  );
}

export default App;
